<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
     x="0px" y="0px"
     width="${node.width_}px" height="${node.height_}px"
     viewBox="${node.margin_x} ${node.margin_y} ${node.width_} ${node.height_}"
     enable-background="new ${node.margin_x} ${node.margin_y}
                        ${node.width_} ${node.height_}"
     xml:space="preserve">
    <g id="SELECTION">
        ${node.toxml()}
    </g>
</svg>