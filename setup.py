from setuptools import setup, find_packages

setup(name='re_utils',
      version='0.3.9',
      author='Max Korinets',
      author_email='mkorinets@gmail.com',
      license='MIT',
      description='Object models for storing and utilities for processing '
                  'architecture data',
      install_requires=[
          "sqlalchemy",
          'mako'
      ],
      include_package_data=True,
      packages=find_packages())
